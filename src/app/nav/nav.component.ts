import {Component, OnInit} from '@angular/core';
import { NAV_ITEMS } from '../mock-nav';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css', '../app.component.css']
})
export class NavComponent implements OnInit {
  private navItems = NAV_ITEMS;
  private selectedNavItem = {};

  constructor() {
  }

  ngOnInit() {
  }

  onSelect(navItem): void {
    this.selectedNavItem = navItem;
  }

}
