export const NAV_ITEMS = [
  {title: 'Define', icon: 'sheet', children: [{title: 'TBD'}]},
  {
    title: 'Gather',
    icon: 'refresh',
    children: [{title: 'Forms'}, {title: 'Audit Results'}, {title: 'Legacy Forms'}, {title: 'Print Blank Form'}]
  },
  {title: 'See', icon: 'cal', children: [{title: 'TBD'}]},
  {title: 'Act', icon: 'wrench', children: [{title: 'TBD'}]},
  {title: 'Admin', icon: 'puzzle', children: [{title: 'TBD'}]}
];
